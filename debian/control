Source: libnet-mac-vendor-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Ansgar Burchardt <ansgar@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13)
Build-Depends-Indep: libcompress-bzip2-perl <!nocheck>,
                     libio-compress-perl <!nocheck>,
                     libio-socket-ssl-perl <!nocheck>,
                     libmojolicious-perl <!nocheck>,
                     libnet-ssleay-perl <!nocheck>,
                     libwww-perl <!nocheck>,
                     perl
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libnet-mac-vendor-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libnet-mac-vendor-perl.git
Homepage: https://metacpan.org/release/Net-MAC-Vendor
Rules-Requires-Root: no

Package: libnet-mac-vendor-perl
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends},
         libio-socket-ssl-perl,
         libmojolicious-perl,
         libnet-ssleay-perl,
         libwww-perl
Recommends: libcompress-bzip2-perl,
            libio-compress-perl
Description: module to look up the vendor by OUI
 The Institute of Electrical and Electronics Engineers (IEEE) assigns an
 Organizational Unique Identifier (OUI) to manufacturers of network
 interfaces. Each interface has a Media Access Control (MAC) address of six
 bytes. The first three bytes are the OUI.
 .
 Net::MAC::Vendor allows you to take a MAC address and turn it into the OUI
 and vendor information. You can, for instance, scan a network, collect MAC
 addresses, and turn those addresses into vendors. With vendor information,
 you can often guess at what you are looking at (e.g. an Apple product).
 .
 You can use this as a module as its individual functions, or call it as a
 script with a list of MAC addresses as arguments. The module can figure it
 out.
