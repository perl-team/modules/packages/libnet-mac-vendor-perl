libnet-mac-vendor-perl (1.268-1) unstable; urgency=medium

  * Team upload.

  [ Debian Janitor ]
  * Update standards version to 4.5.0, no changes needed.

  [ gregor herrmann ]
  * Import upstream version 1.268.
  * Refresh no-network-tests.patch (offset).
  * Declare compliance with Debian Policy 4.6.0.
  * Set Rules-Requires-Root: no.
  * Bump debhelper-compat to 13.
  * Update (build) dependencies.

 -- gregor herrmann <gregoa@debian.org>  Sun, 13 Feb 2022 00:45:27 +0100

libnet-mac-vendor-perl (1.265-1) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * Import upstream version 1.265
  * Update debian/upstream/metadata.
  * debian/copyright:
    - new Upstream-Contact
    - update years of upstream copyright
    - add info about new file
    - replaces tabs with spaces
    - remove trailing whitespace
  * Update build and runtime dependencies.
  * Update no-network-tests.patch.
  * autopkgtests: don't skip smoke test, it works now.
  * Annotate test-only build dependencies with <!nocheck>.
  * Declare compliance with Debian Policy 4.4.1.
  * Bump debhelper-compat to 12.
  * debian/watch: use uscan version 4.

 -- gregor herrmann <gregoa@debian.org>  Thu, 26 Dec 2019 17:35:15 +0100

libnet-mac-vendor-perl (1.262-1) unstable; urgency=medium

  * Team upload.
  * Disable network tests during autopkgtests like during build.
  * Import upstream version 1.262.
  * Update years of upstream copyright.
  * Add (build) dependency on libcompress-bzip2-perl.
  * Update no-network-tests.patch.
  * Declare compliance with Debian Policy 4.1.3.
  * Bump debhelper compatibility level to 10.

 -- gregor herrmann <gregoa@debian.org>  Tue, 02 Jan 2018 01:05:46 +0100

libnet-mac-vendor-perl (1.26-1) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove Matt Zagrabelny from Uploaders. Thanks for your work!
  * Import upstream version 1.26
  * Refresh no-network-tests.patch.
  * Declare compliance with Debian Policy 3.9.8.
  * Update build dependencies.

 -- gregor herrmann <gregoa@debian.org>  Sun, 25 Dec 2016 22:36:22 +0100

libnet-mac-vendor-perl (1.25-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 1.25.
  * Add (build) dependency on libmojolicious-perl.
  * Update no-network-tests.patch. The test suite was restructured a bit.
  * Don't install README* anymore.
  * Update years of upstream copyright and license.
  * Bump debhelper compatibility level to 9.

 -- gregor herrmann <gregoa@debian.org>  Sun, 08 Nov 2015 19:24:49 +0100

libnet-mac-vendor-perl (1.23-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 1.23.
  * Update no-network-tests.patch.

 -- gregor herrmann <gregoa@debian.org>  Thu, 09 Jul 2015 21:37:25 +0200

libnet-mac-vendor-perl (1.22-1) unstable; urgency=medium

  * Team upload.

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ gregor herrmann ]
  * Add debian/upstream/metadata
  * Import upstream version 1.22
  * Refresh no-network-tests.patch.
  * Mark package as autopkgtest-able.
  * Declare compliance with Debian Policy 3.9.6.

 -- gregor herrmann <gregoa@debian.org>  Sat, 18 Oct 2014 22:10:50 +0200

libnet-mac-vendor-perl (1.21-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Update years of upstream copyright.
  * Make short description a noun phrase.
  * Declare compliance with Debian Policy 3.9.5.

 -- gregor herrmann <gregoa@debian.org>  Sat, 11 Jan 2014 21:45:35 +0100

libnet-mac-vendor-perl (1.20-1) unstable; urgency=low

  * Team upload.

  [ Harlan Lieberman-Berg ]
  * Update d/* to latest S-V

  [ Salvatore Bonaccorso ]
  * debian/control: Changed: Replace versioned (build-)dependency on
    perl (>= 5.6.0-{12,16}) with an unversioned dependency on perl (as
    permitted by Debian Policy 3.8.3).
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Ansgar Burchardt ]
  * Email change: Ansgar Burchardt -> ansgar@debian.org
  * debian/control: Convert Vcs-* fields to Git.

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Nuno Carvalho ]
  * New upstream release
    (Closes: #724227)
  * debian/control:
    + update standards version to 3.9.4
    + remove libdbm-deep-perl from D-B-I, not required anymore
    + require Test::More 0.98 in D-B-I
  * debian/copyright:
    + update copyright years
    + cosmetic update license stanzas and entries
  * Update patch to skip tests that required internet
  * debian/rules: add override_dh_auto_test to set NO_NETWORK

 -- Nuno Carvalho <smash@cpan.org>  Tue, 08 Oct 2013 16:03:16 +0100

libnet-mac-vendor-perl (1.18-2) unstable; urgency=low

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ Ansgar Burchardt ]
  * Use minimal debian/rules.
  * New patch "network-tests.patch":
    Do not fail tests if not connected to network (Closes: #534045).
  * Add myself to Uploaders.
  * Bump Standards Version to 3.8.2 (no changes).
  * Add build-dep on libtest-pod-perl, libtest-pod-coverage-perl,
    libdbm-deep-perl to enable additional tests.
  * Remove duplicated word from package description.
  * Do not install unhelpful examples/README.
  * add "makefile.patch" fixing installed POD manual page extension

 -- Ansgar Burchardt <ansgar@43-1.org>  Sun, 21 Jun 2009 19:52:39 +0200

libnet-mac-vendor-perl (1.18-1) unstable; urgency=low

  * Initial Release. (Closes: #501717)

 -- Matt Zagrabelny <mzagrabe@d.umn.edu>  Wed, 8 Apr 2009 13:31:24 -0500
